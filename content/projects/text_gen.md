+++
title = "Kanye West Song Genorator"
date = "2018-06-29T17:25:00-05:00"
tags = ["AI", "Markov Chain", "Song Generator"]
categories = ["AI", "Project", "Python"]
+++

[Link to repository](https://gitlab.com/steveslatky/automaton)

# Why did I make this? 
This was intentionally written for a school assignment to create something with a /brain/. I was listening to the Album *Ye* a lot and I love his word play and verses in it. This made me think how songs are created and could an AI do the same? In the end it came with some success but there is much improvement to be had. 

## Requirements

Python3

## Commands

python3 markov_chain.py

output save in ./songs/n.txt

# What is it? 

Have you ever wanted a new song by an artist but did not want to wait? 
Well I have the solution for you... kind of. I hope that artist is Kanye
West.

This application will take in a group of song lyrics and output a new song
based on the inputted lyrics from [genius](https://genius.com/). This new song will have it's own structure.

# Example output

The songs can be found in the folder songs. There is a note on top indicating what
version (1-5) it is and what *improvement* was made in that version

# How does it work?

## Markov Chains Everywhere! 

### Lyric Markov Chain

The program will take in a folder with sanitized lyrics and find 
the connections between the words. Right now it only does probability 
based on the next word. It does not care what words came before. 

#### How it generates lines

It will choose a word at random to start the line. It will then have 
a probability to choose any word connected to the current word. 
The more times it has seen it in the the inputted lyrics. 

If the current word has no connections to it, the line will just 
end and more on to the next, picking a new word at random. 

#### Improvements

* Have an idea of part of speech E.G. Nouns, adjectives, verbs
* Make prior words have an affect on probability

### Structure Markov Chain

This is where I tried to get creative a bit. In the input files there are tags
for what section of the song it is. E.G. [Chorus], [Verse]. The program will
parse through and count the amount of times it sees a tag. It will also count
the number of lines of each section. It will average the numbers out over
all the songs in the input folder. 


#### How It Generates Sections

In my program a chorus is repeated lines. Verses are new lyrics and you 
probably will not see repeated lines in these sections. Right now there 
are only these two tags. 

With this simple structure of Chorus and Verses it will choose what to start
with based off of the probability of each. It will then flip flop between 
the two based off the number of lines each section was found to be on average. 

#### Improvements

* Read in more tags
* With more tags a more complex structure where it does not just flip flop. 


# Does it do what was intended!? 

**Kind of** 

The songs it makes will not trick any human. There are a few lines that
are pretty good and will make you laugh, but that is mostly it. 

I think with more work and with some of the improvements stated above 
I could get close to tricking someone. Or at least be better than it is. 

## More improvements 

* Have a bigger source of songs. Right now there are only 8 songs
I had to import each song by hand because I did not want to write a
web scrapper at the moment and wanted to focus more on the AI side. 

# Does it show Classical Conditioning 

Kinda. It does *learn* in some way on how songs work but it does not get
stimulus. I do not count the inputted songs as stimulus really. Although 
if more songs are inputted it will change how the song is generated slightly. 

## Could Classical Conditioning be added? 

At the state it is in I don't believe I could add a real classical 
conditioning system with time and strengths. The word strength has 
some sense of conditioning but it is not *smart* in any real sense. 
It does not learn over time. With more lyrics it will get more chances
to get good lines. But that is about it. 

Maybe with an part of speech it would be able to learn and have a 
conditioning. Or if there is a system to tell what is a good line 
and what is a bad line. It could make more connections. I don't 
know how I would design that at this point. 


