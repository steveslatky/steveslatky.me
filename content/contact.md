+++
title = "Contact"
id = "contact"
formspree = true
+++

# Want to talk?  

Are you curious about something? Do you want to get in touch? The best way to get a hold of me is through email. 

I will get back to you as soon as I can. 
